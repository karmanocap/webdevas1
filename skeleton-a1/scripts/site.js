  
'use strict';

/**
 * description here
 * Script for assigment 1 web dev II
 * @author Hamza Jaidi
 */
document.addEventListener("DOMContentLoaded", setup)

function setup () {
    //setup click event
    // setup code
    const btn = document.querySelector("button");
    btn.addEventListener('click', createTable);
}

// your functions
//make table 
function createTable(e) {
    document.querySelector('#tableHere').textContent = undefined;
    let rows = document.querySelector('#numRows');
    let columns = document.querySelector('#numColumns');
    let odd = document.querySelector('#oddColor');
    let even = document.querySelector('#evenColor');
    let table = document.createElement('table'); 
    let tr, td;

    for(let i = 0; i < rows.value; i++) {
        tr = document.createElement('tr');
        

        for (let j = 0; j < columns.value; j++) {
            td = document.createElement('td');
            if((j + i) % 2 == 0){
                tr.style.backgroundColor = even.value;
                
            } 
            else {
                td.style.backgroundColor = odd.value;
            }
        
            tr.appendChild(td);
            td.innerText = `${j + 1}, ${i + 1}`;
        }
        table.appendChild(tr)
    }

    document.querySelector('#tableHere').appendChild(table)


    e.preventDefault();
}

//limit value of entries
function maxValueCheck(numRows, numColumns)
  {
    if (numRows.value.length > numRows.maxLength) {
    numRows.value = numRows.value.slice(0, numRows.maxLength)
    numRows.value = 1;
    }
    if (numColumns.value.length > numColumns.maxLength) {
    numColumns.value = numColumns.value.slice(0, numColumns.maxLength)
    numColumns.value = 1;
    }
  }
